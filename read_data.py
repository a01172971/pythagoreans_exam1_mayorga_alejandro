#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#Function that reads that receives a filename, expecting a .dat file of 14 columns, names each column accordingly to the heart.doc file
#and then returns the dataframe created.
def read(filename):
    df = pd.read_csv(filename, sep="\s+", names=['age','sex', 'chest_pain_type', 'resting_blood_pressure', 'serum_cholestoral_in_mg/dl',
    'fasting_blood_sugar_>_120_mg/dl', 'resting_electrocardiographic_results', 'maximum_heart_rate_achieved', 'exercise_induced_angina',
    'oldpeak', 'the_slope_of_the_peak_exercise_ST_segment', 'number_of_major_vessels', 'thal', 'variable_to_be_predicted'])
    return df

#Function that first, reads the file heart.dat using the read() function in this script, then extracts the value of X from the
#'resting_blood_pressure' column from the dataframe, calculates the value of the variables mu and sigma, and utilize these 3
#variables to calculate the blood pressure deviation(bpd), after that, it computes a new column called "blood_pressure_deviation"
#that is given the value of bpd, finally it writes the raw data from the modified dataframe into a new file called data2.dat
def blood_pressure_deviation():
    df = read('heart.dat')
    x = df['resting_blood_pressure']
    mu = np.mean(df['resting_blood_pressure'])
    sigma = np.std(df['resting_blood_pressure'])
    bpd = (x-mu)/sigma
    df['blood_pressure_deviation'] = bpd
    df.to_csv('data2.dat', sep=" ", header=False, index=False)

def make_plot():
    df = read('heart.dat')
    plt.hist(df['age'])
    plt.show()
    plt.hist(df['age'])
    plt.savefig('histogram_of_ages')


if __name__ == "__main__":
    blood_pressure_deviation()
    make_plot()
